package kz.attractor.microgram.Model;

import org.springframework.data.repository.CrudRepository;

public interface ILikeItRepository extends CrudRepository<ILikeIt, String> {
}
