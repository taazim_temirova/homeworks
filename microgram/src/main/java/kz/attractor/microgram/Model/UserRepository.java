package kz.attractor.microgram.Model;

import org.springframework.data.domain.Sort;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User,String> {

    public Iterable<User> findAllById(Sort s);

    public Iterable<User> findAllById(int year, Sort s);

}