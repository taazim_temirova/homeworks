package kz.attractor.microgram.Model;

import org.springframework.data.repository.CrudRepository;

public interface UserSubscriptionsRepository extends CrudRepository<UserSubscriptions, String> {
}
