package kz.attractor.microgram.Controller;

import kz.attractor.microgram.Model.User;
import kz.attractor.microgram.Model.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;


@Controller
public class UserController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/user/{id}")
    public  Iterable<User> getId(@PathVariable("id") int id){
        Sort s = Sort.by(Sort.Order.asc("id"));
        return userRepository.findAllById(id,s);
    }
}
